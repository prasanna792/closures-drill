
function cacheFunction(cb) {
    if (typeof cb !== 'function') {
        throw new Error('Invalid parameter: cb must be a function.');
    }

    let cache = {};

    return function invokeCallBack(...args) {
        const argsString = JSON.stringify(args);
        if (cache[argsString]) {
            return cache[argsString];
        } else {
            const result = cb(...args);
            cache[argsString] = result;
            return result;
        }
    };
}

module.exports = cacheFunction;
