

function limitFunctionCallCount(cb, n) {
    if (typeof cb !== 'function' || typeof n !== 'number' || n <= 0) {
        throw new Error('Invalid parameters: cb must be a function, and n must be a positive number.');
    }

    let count = 0;

    return function invokeCallBackFunction(...args) {
        if (count < n) {
            count++;
            return cb(...args);
        } else {
            return null;
        }
    };
}

module.exports = limitFunctionCallCount;

