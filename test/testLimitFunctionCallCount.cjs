
const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function hello(name) {
    return `Hello, ${name}!`;
}

const result = limitFunctionCallCount(hello, 6);

try {
    console.log(result());        
    console.log(result('lakshmi')); 
    console.log(result('prasanna')); 
    console.log(result());       
    console.log(result('hii'));   
    console.log(result());       
    console.log(result());       
} catch (error) {
    console.error('Error:', error.message);
}