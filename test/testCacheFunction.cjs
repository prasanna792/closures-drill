const cacheFunction = require('../cacheFunction.cjs');

function callback(...args) {
    return args;
}

const result = cacheFunction(callback);

try {
    console.log(result(1, 2, 3)); 
    console.log(result(1, 2, 3)); 
    console.log(result(4, 5, 6)); 
    console.log(result(1, 2, 3)); 
} catch (error) {
    console.error('Error:', error.message);
}