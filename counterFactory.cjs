function counterFactory() {
    let count = 0;
    let incrementAndDecrementMethods;
    return incrementAndDecrementMethods = {
        increment: () => {
            return count += 1;
        },
        decrement: () => {
            return count = count - 1;
        }
    }
}


module.exports = counterFactory;